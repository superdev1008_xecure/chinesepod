package llc.chinesepod.service;

import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.support.v4.media.MediaDescriptionCompat;
import android.support.v4.media.MediaMetadataCompat;
import android.support.v4.media.session.MediaSessionCompat;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.android.exoplayer2.DefaultControlDispatcher;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.PlaybackParameters;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.ext.mediasession.MediaSessionConnector;
import com.google.android.exoplayer2.ext.mediasession.TimelineQueueNavigator;
import com.google.android.exoplayer2.source.ConcatenatingMediaSource;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.ui.PlayerNotificationManager;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.upstream.cache.CacheDataSource;
import com.google.android.exoplayer2.upstream.cache.CacheDataSourceFactory;
import com.google.android.exoplayer2.util.Util;

import java.io.File;

import llc.chinesepod.ChinesePodApplication;
import llc.chinesepod.Config;
import llc.chinesepod.Utils.DownloadUtil;
import llc.chinesepod.recap.ExoPlayerActivity;
import llc.chinesepod.recap.MainActivity;
import llc.chinesepod.recap.R;

import static llc.chinesepod.Config.MEDIA_SESSION_TAG;
import static llc.chinesepod.Config.PLAYBACK_NOTIFICATION_ID;

public class AudioPlayerService extends Service {

    private SimpleExoPlayer player;
    private PlayerNotificationManager playerNotificationManager;
    private MediaSessionCompat mediaSession;
    private MediaSessionConnector mediaSessionConnector;

    private Uri uri;
    private String title;
    private String description;
    private int idBitmap;
    private long seek;

    public static AudioPlayerService instance;
    private boolean isRecycle;

    public static AudioPlayerService getInstance(){
        return instance;
    }

    public void playAudio(){
        final Context context = this;
        player = ExoPlayerFactory.newSimpleInstance(context, new DefaultTrackSelector());
        DefaultDataSourceFactory dataSourceFactory = new DefaultDataSourceFactory(
                context, Util.getUserAgent(context, getString(R.string.app_name)));
        CacheDataSourceFactory cacheDataSourceFactory = new CacheDataSourceFactory(
                DownloadUtil.getCache(context),
                dataSourceFactory,
                CacheDataSource.FLAG_IGNORE_CACHE_ON_ERROR);
        ConcatenatingMediaSource concatenatingMediaSource = new ConcatenatingMediaSource();

        MediaSource mediaSource = new ExtractorMediaSource.Factory(cacheDataSourceFactory)
                .createMediaSource(uri);
        concatenatingMediaSource.addMediaSource(mediaSource);

        player.prepare(concatenatingMediaSource);
        player.setPlayWhenReady(true);
        //to seek
        player.seekTo(seek);

        playerNotificationManager = PlayerNotificationManager.createWithNotificationChannel(
                context,
                Config.PLAYBACK_CHANNEL_ID,
                R.string.playback_channel_name,
                PLAYBACK_NOTIFICATION_ID,
                new PlayerNotificationManager.MediaDescriptionAdapter() {
                    @Override
                    public String getCurrentContentTitle(Player player) {
                        return title;
                    }

                    @Nullable
                    @Override
                    public PendingIntent createCurrentContentIntent(Player player) {
                        Intent intent = new Intent(context, ExoPlayerActivity.class);
                        return PendingIntent.getActivity(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
                    }

                    @Nullable
                    @Override
                    public String getCurrentContentText(Player player) {
                        return description;
                    }

                    @Nullable
                    @Override
                    public Bitmap getCurrentLargeIcon(Player player, PlayerNotificationManager.BitmapCallback callback) {
                        return ((BitmapDrawable) context.getResources().getDrawable(idBitmap)).getBitmap();
                    }
                }
        );
        playerNotificationManager.setNotificationListener(new PlayerNotificationManager.NotificationListener() {
            @Override
            public void onNotificationStarted(int notificationId, Notification notification) {
                startForeground(notificationId, notification);
            }

            @Override
            public void onNotificationCancelled(int notificationId) {
                stopSelf();
            }
        });
        playerNotificationManager.setPlayer(player);
        playerNotificationManager.setFastForwardIncrementMs(5000);
        playerNotificationManager.setUseNavigationActions(false);

        mediaSession = new MediaSessionCompat(context, MEDIA_SESSION_TAG);
        mediaSession.setActive(true);
        playerNotificationManager.setMediaSessionToken(mediaSession.getSessionToken());

        mediaSessionConnector = new MediaSessionConnector(mediaSession);
        mediaSessionConnector.setQueueNavigator(new TimelineQueueNavigator(mediaSession) {
            @Override
            public MediaDescriptionCompat getMediaDescription(Player player, int windowIndex) {
                return AudioPlayerService.this.getMediaDescription(context, idBitmap, title, description);
            }
        });
        mediaSessionConnector.setPlayer(player, null);

        playerNotificationManager.setControlDispatcher(new DefaultControlDispatcher(){
            @Override
            public boolean dispatchStop(Player player, boolean reset) {
                SharedPreferences sharedPreferences = getApplication().getSharedPreferences("audio_info", MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPreferences.edit();

                editor.putLong(uri.toString(), player.getContentPosition());

                editor.commit();

                //clear internal storage
                SharedPreferences specialRefer = getSharedPreferences("special", MODE_PRIVATE);
                String dir_name = specialRefer.getString("lesson_directory", null);
                if(dir_name == null || DownloadService.instance != null){
                    return super.dispatchStop(player, reset);
                }

                File directory = getFilesDir();
                File[] files = directory.listFiles();
                for (File audio_dir: files){
                    if(audio_dir.getName().compareTo(dir_name) == 0 || audio_dir.getName().compareTo(".Fabric") == 0){
                        continue;
                    }
                    if (audio_dir.isDirectory())
                    {
                        String[] children = audio_dir.list();
                        for (int i = 0; i < children.length; i++)
                        {
                            new File(audio_dir, children[i]).delete();
                        }
                    }
                    audio_dir.delete();
                }

                return super.dispatchStop(player, reset);
            }
        });
    }

    private MediaDescriptionCompat getMediaDescription(Context context, int idBitmap, String  title, String description) {
        Bundle extras = new Bundle();
        Bitmap bitmap = ((BitmapDrawable) context.getResources().getDrawable(idBitmap)).getBitmap();;
        extras.putParcelable(MediaMetadataCompat.METADATA_KEY_ALBUM_ART, bitmap);
        extras.putParcelable(MediaMetadataCompat.METADATA_KEY_DISPLAY_ICON, bitmap);
        return new MediaDescriptionCompat.Builder()
                .setMediaId("0")
                .setIconBitmap(bitmap)
                .setTitle(title)
                .setDescription(description)
                .setExtras(extras)
                .build();
    }

    @Override
    public void onCreate() {
        super.onCreate();

        instance = this;

        SharedPreferences sharedPreferences = getApplication().getSharedPreferences("audio_info", MODE_PRIVATE);

        ChinesePodApplication app = (ChinesePodApplication)getApplication();
        if (app.currentUrl == null){
            return;
        }
        uri = Uri.parse(app.currentUrl);
        title = sharedPreferences.getString("title", "");
        description = sharedPreferences.getString("description", "");
        idBitmap = sharedPreferences.getInt("bitMap", R.drawable.logo_l);
        seek = sharedPreferences.getLong(uri.toString(), 0);

        isRecycle = app.isRecycle;

        playAudio();

        Float speed_value = app.speed_value;
        if(speed_value != null){
            PlaybackParameters param = new PlaybackParameters(speed_value);
            player.setPlaybackParameters(param);
        }
    }

    @Override
    public void onDestroy() {
        if (mediaSession != null){
            mediaSession.release();
            mediaSessionConnector.setPlayer(null, null);
            playerNotificationManager.setPlayer(null);
            player.release();
            player = null;
        }
        instance = null;

        super.onDestroy();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return START_STICKY;
    }

    private Handler mGetSeeking = new Handler(){
        @Override
        public void handleMessage(@NonNull Message msg) {
            super.handleMessage(msg);

            long max = player.getDuration();
            long current = player.getCurrentPosition();
            if(player != null && isRecycle && current > max - 2000){
                player.seekTo(0);
            }

            mGetSeeking.sendEmptyMessageDelayed(0, 10);
        }
    };

}
