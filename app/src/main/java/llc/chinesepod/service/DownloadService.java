package llc.chinesepod.service;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.app.TaskStackBuilder;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.IBinder;
import android.os.Message;
import android.os.VibrationEffect;
import android.os.Vibrator;
import android.util.ArrayMap;
import android.util.Log;

import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Map;

import llc.chinesepod.ChinesePodApplication;
import llc.chinesepod.Config;
import llc.chinesepod.Utils.ScheduleUtil;
import llc.chinesepod.fm.MainFragment;
import llc.chinesepod.recap.MainActivity;
import llc.chinesepod.recap.R;

import static llc.chinesepod.Config.JOB_REPEAT_TIME;

public class DownloadService extends Service {

    final String LOG = "RecapDownload";
    static public DownloadService instance;

    private static SharedPreferences specialRefer, audioCheckPrf;

    private String lesson_id = null,
            mainBK = null,
            lessonTitle = null,
            emailAddress = null,
            charSet = null,
            subscription = null;

    @Override
    public void onCreate() {
        super.onCreate();
        specialRefer = getSharedPreferences("special", MODE_PRIVATE);
        audioCheckPrf = getSharedPreferences("audioCheckPrf", MODE_PRIVATE);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        instance = this;
        lesson_id = intent.getStringExtra("lesson_id");
        mainBK = intent.getStringExtra("mainBK");
        lessonTitle = intent.getStringExtra("lessonTitle");
        emailAddress = intent.getStringExtra("emailAddress");
        charSet = intent.getStringExtra("charSet");
        subscription = intent.getStringExtra("subscription");

        ArrayList<String> keys = intent.getStringArrayListExtra("keys");
        if (keys == null){
            stopSelf();
            return Service.START_NOT_STICKY;
        }

        ArrayMap<String, String> files = new ArrayMap<>();
        for (String key: keys){
            files.put(key, intent.getStringExtra(key));
        }

        startDownload(files);

        return Service.START_NOT_STICKY;
    }

    @Override
    public void onDestroy() {
        ScheduleUtil.scheduleJob(getApplicationContext(), JOB_REPEAT_TIME);
        super.onDestroy();
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    private void startDownload(ArrayMap<String, String> files) {
        Thread thread = new Thread() {
            @Override
            public void run() {
                boolean success = true;

                int fileLength = 0;
                for (Map.Entry<String, String> entry : files.entrySet()) {
                    try {
                        System.out.println(entry.getKey());

                        String fileUrl = entry.getKey();
                        URL url = new URL(fileUrl);
                        URLConnection connection = url.openConnection();
                        connection.connect();
                        fileLength += connection.getContentLength();
                    } catch (Exception e) {
                        Log.e(LOG, entry.getKey());
                        success = false;
                        try {
                            Message msg = new Message();
                            msg.arg1 = 1;
                            MainFragment.instance.showNotify.sendMessage(msg);
                        } catch (Exception error){
                            error.printStackTrace();
                        }
                        instance = null;
                        stopSelf();
                        return;
                    }
                }
                long total = 0;
                int count, tmpPercentage = 0;
                Long tsLong = System.currentTimeMillis()/1000;
                File dir = new File(getApplicationContext().getFilesDir() + "/" + tsLong.toString());
                if (!dir.exists())
                    dir.mkdir();
                System.out.println(tsLong);

                for (Map.Entry<String, String> entry : files.entrySet()) {
                    try {
                        System.out.println("downloading... " + entry.getKey());
                        String fileUrl = entry.getKey();

                        String fileName = entry.getValue();
                        File tmpFile = new File(dir + fileName);

                        URL url = new URL(fileUrl);
//                        URLConnection connection = url.openConnection();
//                        connection.connect();
                        // input stream to read file - with 8k buffer
                        InputStream input = new BufferedInputStream(url.openStream(), 8192);

                        // Output stream to write file

                        OutputStream output = new FileOutputStream(tmpFile);

                        byte data[] = new byte[256];
                        while ((count = input.read(data)) != -1) {
                            total += count;
                            int percentage = (int) ((total * 100) / (fileLength));
                            output.write(data, 0, count);
                            if (percentage > tmpPercentage) {
                                tmpPercentage = percentage;
                            }

                            Message msg = new Message();
                            msg.arg1 = percentage;
                            try {
                                MainFragment.instance.showProgress.sendMessage(msg);
                                Message msg_ = new Message();
                                msg_.arg1 = 5;
                                msg_.arg2 = percentage;
                                MainFragment.instance.showNotify.sendMessage(msg_);
                            }catch (Exception e){
                                e.printStackTrace();
                            }
                        }

                        // flushing output
                        output.flush();

                        // closing streams
                        output.close();
                        input.close();
                    } catch (Exception e) {
                        Log.e("downloading... " + entry.getKey(), e.getMessage());
                        success = false;
                        try {
                            Message msg = new Message();
                            msg.arg1 = 1;
                            MainFragment.instance.showNotify.sendMessage(msg);
                        }catch (Exception e1){
                            e1.printStackTrace();
                        }
                        break;
                    }
                }
                String uriBK = null;
                if (success && mainBK != ""){
                    uriBK = saveBK(mainBK, dir);
                } else if (success && mainBK == ""){
                    uriBK = null;
                }

                Message msg = new Message();

                if (success){
                    msg.arg1 = 1;
                    SharedPreferences.Editor editor = specialRefer.edit();
                    editor.putBoolean("save", true);
                    editor.putString("lesson_id", lesson_id);
                    editor.putString("lesson_directory", tsLong.toString());
                    editor.putString("mainBK", uriBK);
                    editor.putString("lessonTitle", lessonTitle);
                    editor.putString("emailAddress", emailAddress);
                    editor.putString("charSet", charSet);
                    editor.putString("subscription", subscription);
                    editor.putBoolean("no_recap", false);
                    editor.commit();
                    //check lesson
                    SharedPreferences.Editor editor1 = audioCheckPrf.edit();
                    Long currentTime = Calendar.getInstance().getTimeInMillis();
                    editor1.putLong("time", currentTime);
                    editor1.commit();

                    NotificationNewLesson();

                    // cancel manual update
                    ChinesePodApplication app = (ChinesePodApplication) getApplication();
                    app.manual_download = false;

                    //clear internal storage
                    SharedPreferences specialRefer = getSharedPreferences("special", MODE_PRIVATE);
                    String dir_name = specialRefer.getString("lesson_directory", null);

                    if (AudioPlayerService.getInstance() == null){
                        File directory = getFilesDir();
                        File[] files = directory.listFiles();
                        for (File audio_dir: files){
                            if(audio_dir.getName().compareTo(dir_name) == 0 || audio_dir.getName().compareTo(".Fabric") == 0){
                                continue;
                            }
                            if (audio_dir.isDirectory())
                            {
                                String[] children = audio_dir.list();
                                for (int i = 0; i < children.length; i++)
                                {
                                    new File(audio_dir, children[i]).delete();
                                }
                            }
                            audio_dir.delete();
                        }
                    }
                    try{
                        MainActivity.instance.showHamburger.sendEmptyMessage(0);
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }else{
                    msg.arg1 = 0;
                }
                try{
                    MainFragment.instance.stopProgress.sendMessage(msg);
                    MainFragment.instance.setBackground.sendEmptyMessage(0);
                }catch (Exception e){
                    e.printStackTrace();
                }
                instance = null;
                stopSelf();
            }
        };
        thread.start();
    }

    private String saveBK(String mainBK, File dir) {
        File tmpFile = null;
        try {
            System.out.println(mainBK);

            tmpFile = new File(dir + "/background");

            URL url = new URL(mainBK);
            URLConnection conection = url.openConnection();
            conection.connect();

            // input stream to read file - with 8k buffer
            InputStream input = new BufferedInputStream(url.openStream(), 8192);

            // Output stream to write file

            OutputStream output = new FileOutputStream(tmpFile);

            byte data[] = new byte[1024];
            int count = 0;
            while ((count = input.read(data)) != -1) {
                output.write(data, 0, count);
            }

            // flushing output
            output.flush();

            // closing streams
            output.close();
            input.close();
        } catch (Exception e) {
            try{
                Message msg = new Message();
                msg.arg1 = 6;
                MainFragment.instance.showNotify.sendMessage(msg);
            }catch (Exception e1){
                e1.printStackTrace();
            }
        }
        return Uri.fromFile(tmpFile).toString();
    }

    private void NotificationNewLesson(){
        NotificationManagerCompat notificationManagerCompat = NotificationManagerCompat.from(getApplicationContext());

        notificationManagerCompat.cancel(Config.NEW_LESSON);

        String contentTitle = "ChinesePod Recap";
        Intent notifyIntent = new Intent();
        PendingIntent notifyPendingIntent = PendingIntent.getActivity(getApplicationContext(), Config.NEW_LESSON, notifyIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        NotificationCompat.Builder notificationBuilder = createNotificationBuilder("lesson_channel");
        notificationBuilder.setContentIntent(notifyPendingIntent);
        notificationBuilder.setOngoing(false);
        notificationBuilder.setAutoCancel(false);
        notificationBuilder.setSmallIcon(R.drawable.logo_l);
        notificationBuilder.setContentTitle(contentTitle);
        notificationBuilder.setContentText("New lesson (" + lesson_id + ") is available.");
        Uri alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        notificationBuilder.setSound(alarmSound);

        // Create an Intent for the activity you want to start
        Intent resultIntent = new Intent(DownloadService.this, MainActivity.class);
        // Create the TaskStackBuilder and add the intent, which inflates the back stack
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(DownloadService.this);
        stackBuilder.addNextIntentWithParentStack(resultIntent);
        // Get the PendingIntent containing the entire back stack
        PendingIntent resultPendingIntent =
                stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
        notificationBuilder.setContentIntent(resultPendingIntent);

        notificationManagerCompat.notify(Config.NEW_LESSON, notificationBuilder.build());

        vibrate();
        Uri uri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        Ringtone ringtone = RingtoneManager.getRingtone(getApplicationContext(),uri);
        ringtone.play();
    }

    private NotificationCompat.Builder createNotificationBuilder(String channelId) {
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            String channelName = getString(R.string.app_name);
            NotificationChannel notificationChannel = new NotificationChannel(channelId, channelName, NotificationManager.IMPORTANCE_LOW);
            notificationChannel.setLightColor(Color.BLUE);
            notificationChannel.setLockscreenVisibility(Notification.VISIBILITY_PRIVATE);
            NotificationManager notificationManager = (NotificationManager)getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);
            if (notificationManager != null) {
                notificationManager.createNotificationChannel(notificationChannel);
            }
        }
        return new NotificationCompat.Builder(getApplicationContext(), channelId);
    }

    private void vibrate() {
        Vibrator vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
        // Vibrate for 500 milliseconds
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            vibrator.vibrate(VibrationEffect.createOneShot(500, VibrationEffect.DEFAULT_AMPLITUDE));
        } else {
            //deprecated in API 26
            vibrator.vibrate(500);
        }
    }
}
