package llc.chinesepod.service;

import android.app.Service;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Message;
import android.util.Log;

import com.amazonaws.auth.CognitoCachingCredentialsProvider;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.ObjectListing;
import com.amazonaws.services.s3.model.S3ObjectSummary;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import llc.chinesepod.ChinesePodApplication;
import llc.chinesepod.Config;
import llc.chinesepod.Utils.ScheduleUtil;
import llc.chinesepod.fm.MainFragment;
import llc.chinesepod.recap.MainActivity;
import okhttp3.Call;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class RecapService extends Service {
    private static final String TAG = "RecapJobService";

    private static SharedPreferences specialRefer, audioCheckPrf;
    private String lesson_id;
    private long differentTime = 180;

    private int _sec = 5;

    static public RecapService instance = null;

    @Override
    public void onCreate() {
        super.onCreate();

        specialRefer = getSharedPreferences("special", MODE_PRIVATE);
        lesson_id = specialRefer.getString("lesson_id", null);
        //check lesson
        audioCheckPrf = getSharedPreferences("audioCheckPrf", MODE_PRIVATE);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        instance = this;

        Log.d(TAG, "startService");
        _sec = intent.getIntExtra("sec", 10);

        Long currentTime = Calendar.getInstance().getTimeInMillis();
        Long savedTime = audioCheckPrf.getLong("time", 0);

        ChinesePodApplication app = (ChinesePodApplication) getApplication();
        boolean auto_download = app.auto_download;
        boolean wifi_download = app.wifi_download;
        boolean manual_download = app.manual_download;
        WifiManager wifiManager = (WifiManager) getApplicationContext().getSystemService(WIFI_SERVICE);
        boolean wifiEnabled = wifiManager.isWifiEnabled();

        if (manual_download && DownloadService.instance == null){
            fetchLessonId();
            Log.d(TAG, "manual_fetch");
        } else if (wifi_download && wifiEnabled){
            fetchLessonId();
            Log.d(TAG, "only_wifi_fetch");
        } else if(wifi_download && !wifiEnabled){
            ScheduleUtil.stopService(getApplicationContext());
            Log.d(TAG, "no_fetch");
            //        } else if (auto_download && DownloadService.instance == null && currentTime - savedTime > differentTime){
        } else if (auto_download && DownloadService.instance == null){
            fetchLessonId();
            Log.d(TAG, "fetchLessonId");
        } else{
            ScheduleUtil.stopService(getApplicationContext());
            Log.d(TAG, "no_fetch");
        }

        return Service.START_NOT_STICKY;
    }

    @Override
    public void onDestroy() {
        Log.d(TAG, "onDestroy");
        ChinesePodApplication app = (ChinesePodApplication) getApplication();
        ScheduleUtil.scheduleJob(getApplicationContext(), _sec);
        super.onDestroy();
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    private void fetchLessonId() {
        SharedPreferences sharedPreferences = getSharedPreferences("user_info", MODE_PRIVATE);
        String session_id = sharedPreferences.getString("sessionid", "");

        if(session_id == null){
            return;
        }

        try {
            if (DownloadService.instance == null){
                Message msg = new Message();
                msg.arg1 = 2;
                MainFragment.instance.showNotify.sendMessage(msg);
            }
        }catch (Exception e){
            e.printStackTrace();
        }

        OkHttpClient client = new OkHttpClient();

        RequestBody formBody = new FormBody.Builder()
                .add("sessionId", session_id)
                .build();

        Request request = new Request.Builder()
                .url(Config.GET_LESSON_ID)
                .post(formBody)
                .header("Content-Type", "application/x-www-form-urlencoded")
                .build();

        client.newCall(request).enqueue(new okhttp3.Callback() {

            @Override
            public void onFailure(Call call, IOException e) {
                String mMessage = e.getMessage().toString();
                Log.w("failure Response", mMessage);
                try {
                    Message msg = new Message();
                    msg.arg1 = 1;
                    MainFragment.instance.showNotify.sendMessage(msg);

                    Message msg_ = new Message();
                    msg_.arg1 = 0;
                    MainFragment.instance.stopProgress.sendMessage(msg_);
                }catch (Exception e1){
                    e1.printStackTrace();
                }
                ScheduleUtil.stopService(getApplicationContext());
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {

                String tmpId = specialRefer.getString("lesson_id", null);
                String jsonData = response.body().string();
                try {
                    JSONObject obj = new JSONObject(jsonData);
                    if (tmpId == null || !(tmpId != null && tmpId.compareTo(obj.getString("lessonId")) == 0 && specialRefer.getBoolean("save", false))){
                        lesson_id = obj.getString("lessonId");
                        String lessonTitle = obj.getString("lessonTitle");
                        String emailAddress = obj.getString("emailAddress");
                        String charSet = obj.getString("charSet");
                        String subscription = obj.getString("subscription");
                        new aws3Task().execute(lesson_id, lessonTitle, emailAddress, charSet, subscription);

                        try {
//                            Message msg = new Message();
//                            msg.arg1 = 3;
//                            MainFragment.instance.showNotify.sendMessage(msg);
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    } else{
                        ScheduleUtil.stopService(getApplicationContext());
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    try {
                        SharedPreferences.Editor editor = specialRefer.edit();
                        editor.putString("mainBK", saveBK(Config.NO_RECAP, getFilesDir()));
                        editor.putString("lesson_id", null);
                        editor.putBoolean("no_recap", true);
                        editor.commit();
                        Message msg = new Message();
                        Bundle bundle = new Bundle();
                        bundle.putString("lesson_id", lesson_id);
                        msg.setData(bundle);
                        MainFragment.instance.NoRecapHandle.sendMessage(msg);
                    }catch (Exception e1){
                        e1.printStackTrace();
                    }
                    ScheduleUtil.stopService(getApplicationContext());
                }
                response.close();
            }
        });
    }

    private class aws3Task extends AsyncTask<String, Void, Void> {
        @Override
        protected Void doInBackground(String... params) {
            String lesson_id = params[0];
            String lessonTitle = params[1];
            String emailAddress = params[2];
            String charSet = params[3];
            String subscription = params[4];
            try {
                CognitoCachingCredentialsProvider credentialsProvider = new CognitoCachingCredentialsProvider(
                        getApplicationContext(),
                        "us-east-1:b475dca2-71f5-4cb0-9ecc-bcf5c4051d5e",
                        Regions.US_EAST_1 // Region enum
                );

                AmazonS3Client s3Client = new AmazonS3Client(credentialsProvider);
                ObjectListing listing = s3Client.listObjects("chinesepod-recap");
                List<S3ObjectSummary> listSummery = listing.getObjectSummaries();

                ArrayList<String> files = new ArrayList<>();
                Intent intentDownloadService = new Intent(getApplicationContext(), DownloadService.class);
                String mainBK = null;
                ArrayList<String> arrayBK = new ArrayList<>();
                for (int i = 0; i < listSummery.size(); i++) {
                    S3ObjectSummary objectSummary = listSummery.get(i);
                    String bucketKey = objectSummary.getKey();
                    if (bucketKey.contains(lesson_id)
                            && (bucketKey.contains(".mp3")
                            || bucketKey.contains(".wav")
                            || bucketKey.contains(".png")
                            || bucketKey.contains(".jpg"))
                            && !bucketKey.contains("background")){
                        files.add(Config.BASE_AUDIO_URL + bucketKey);
                        intentDownloadService.putExtra(Config.BASE_AUDIO_URL + bucketKey, bucketKey.substring(lesson_id.length()));
                    }else if(bucketKey.contains(lesson_id) && bucketKey.contains("background")){
                        arrayBK.add(bucketKey);
                    }
                }
                String CharSet = charSet.compareTo("simplified") == 0 ? "s" : "t";
                for (String bk : arrayBK){
                    if (bk.contains("background." + CharSet)){
                        mainBK = bk;
                        break;
                    }
                }
                if (mainBK == null){
                    try {
                        mainBK = arrayBK.get(0);
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }
                if(files.size() > 0 && DownloadService.instance == null){
                    intentDownloadService.putStringArrayListExtra("keys", files);
                    intentDownloadService.putExtra("lesson_id", lesson_id);
                    intentDownloadService.putExtra("mainBK", mainBK);
                    intentDownloadService.putExtra("lessonTitle", lessonTitle);
                    intentDownloadService.putExtra("emailAddress", emailAddress);
                    intentDownloadService.putExtra("charSet", charSet);
                    intentDownloadService.putExtra("subscription", subscription);

                    startService(intentDownloadService);

                    SharedPreferences.Editor editor = specialRefer.edit();
                    editor.putBoolean("no_recap", false);
                    editor.commit();
                } else if (files.size() <= 0){
                    SharedPreferences.Editor editor = specialRefer.edit();
                    editor.putString("mainBK", saveBK(Config.NO_RECAP, getFilesDir()));
                    editor.putString("lesson_id", null);
                    editor.putBoolean("no_recap", true);
                    editor.commit();
                    try {
                        Message msg = new Message();
                        Bundle bundle = new Bundle();
                        bundle.putString("lesson_id", lesson_id);
                        msg.setData(bundle);
                        MainFragment.instance.NoRecapHandle.sendMessage(msg);
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
                try {
                    Message msg = new Message();
                    msg.arg1 = 1;
                    MainFragment.instance.showNotify.sendMessage(msg);
                }catch (Exception e1){
                    e1.printStackTrace();
                }
            }

            ScheduleUtil.stopService(getApplicationContext());
            return null;
        }
    }

    private String saveBK(String mainBK, File dir) {
        File tmpFile = null;
        try {
            System.out.println(mainBK);

            tmpFile = new File(dir + "/background");

            URL url = new URL(mainBK);
            URLConnection conection = url.openConnection();
            conection.connect();

            // input stream to read file - with 8k buffer
            InputStream input = new BufferedInputStream(url.openStream(), 8192);

            // Output stream to write file

            OutputStream output = new FileOutputStream(tmpFile);

            byte data[] = new byte[1024];
            int count = 0;
            while ((count = input.read(data)) != -1) {
                output.write(data, 0, count);
            }

            // flushing output
            output.flush();

            // closing streams
            output.close();
            input.close();
        } catch (Exception e) {
            try {
                Message msg = new Message();
                msg.arg1 = 6;
                MainFragment.instance.showNotify.sendMessage(msg);
            }catch (Exception e1){
                e1.printStackTrace();
            }
        }
        return Uri.fromFile(tmpFile).toString();
    }
}
