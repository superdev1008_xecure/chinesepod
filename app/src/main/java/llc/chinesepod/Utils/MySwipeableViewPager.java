package llc.chinesepod.Utils;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;

import androidx.viewpager.widget.ViewPager;

public class MySwipeableViewPager extends ViewPager {
    private boolean b_Swip = true;

    public void enableSwip(Boolean b){
        b_Swip = b;
    }

    public MySwipeableViewPager(Context context) {
        super(context);
    }

    public MySwipeableViewPager(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent event) {
        // Never allow swiping to switch between pages
        return b_Swip? super.onInterceptTouchEvent(event) : false;
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        // Never allow swiping to switch between pages
        return b_Swip ? super.onTouchEvent(event) : false;
    }
}
