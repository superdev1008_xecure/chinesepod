package llc.chinesepod.Utils;

import android.app.job.JobInfo;
import android.app.job.JobScheduler;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.net.wifi.WifiManager;
import android.os.Handler;
import android.os.Message;
import android.os.PersistableBundle;

import androidx.annotation.NonNull;

import llc.chinesepod.ChinesePodApplication;
import llc.chinesepod.service.RecapJobService;
import llc.chinesepod.service.RecapService;

import static android.content.Context.WIFI_SERVICE;
import static llc.chinesepod.Config.RECAP_JOB_SERVICE_ID;

public class ScheduleUtil {
    public static void scheduleJob(Context context,  int sec){
        ComponentName serviceComponent = new ComponentName(context, RecapJobService.class);
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
            PersistableBundle bundle = new PersistableBundle();
            bundle.putInt("sec", sec);
            JobInfo.Builder builder = new JobInfo.Builder(RECAP_JOB_SERVICE_ID, serviceComponent)
                    .setMinimumLatency(sec * 1000)
                    .setOverrideDeadline(sec * 1000)
                    .setPersisted(true)
                    .setExtras(bundle);
            JobScheduler jobScheduler = context.getSystemService(JobScheduler.class);
            jobScheduler.schedule(builder.build());
        } else if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            PersistableBundle bundle = new PersistableBundle();
            bundle.putInt("sec", sec);
            JobInfo.Builder builder = new JobInfo.Builder(RECAP_JOB_SERVICE_ID, serviceComponent)
                    .setMinimumLatency(sec * 1000)
                    .setOverrideDeadline(sec * 1000)
                    .setPersisted(true)
                    .setExtras(bundle);
            JobScheduler jobScheduler =
                    (JobScheduler) context.getSystemService(Context.JOB_SCHEDULER_SERVICE);
            jobScheduler.schedule(builder.build());
        } else {
            new Handler(){
                @Override
                public void handleMessage(@NonNull Message msg) {
                    if (RecapService.instance == null){
                        Intent recapService = new Intent(context, RecapService.class);
                        recapService.putExtra("sec", sec);
                        context.startService(recapService);
                    }
                }
            }.sendEmptyMessageDelayed(0, sec * 1000);
        }
    }

    public static void stopService(Context context){
        JobScheduler jobScheduler = null;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
            jobScheduler = context.getSystemService(JobScheduler.class);
            jobScheduler.cancel(RECAP_JOB_SERVICE_ID);
        } else if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            jobScheduler = (JobScheduler) context.getSystemService(Context.JOB_SCHEDULER_SERVICE);
            jobScheduler.cancel(RECAP_JOB_SERVICE_ID);
        } else {
            try{
                RecapService.instance.stopSelf();
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }
}
