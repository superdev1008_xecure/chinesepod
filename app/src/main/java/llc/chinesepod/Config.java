package llc.chinesepod;

public class Config {
    public static String BASE_AUDIO_URL = "https://chinesepod-recap.s3.amazonaws.com/";

    public static String BASE_API_URL = "https://ws.chinesepod.com:444/";
    public static String GET_CODE_URL = BASE_API_URL + "1.0.0/instances/prod/device/get-code";
    public static String CHECK_CODE_URL = BASE_API_URL + "1.0.0/instances/prod/device/check-code";
    public static String CHECK_SESSION_ID = BASE_API_URL + "1.0.0/instances/prod/main/check-sessionid";
    public static String GET_LESSON_ID = "https://www.chinesepod.com/api/v1/recap/get-lessons";
    public static String LOGIN = BASE_API_URL + "1.0.0/instances/prod/main/login";
    public static String NO_RECAP = BASE_AUDIO_URL + "Not.Available.jpg";

    public static final String PLAYBACK_CHANNEL_ID = "playback_channel";
    public static final int PLAYBACK_NOTIFICATION_ID = 1;
    public static final int DOWNLOAD_NOTIFICATION_ID = 2;
    public static final int NEW_LESSON = 3;

    public static final int RECAP_JOB_SERVICE_ID = 4;

    public static final String MEDIA_SESSION_TAG = "audio_demo";
    public static final String DOWNLOAD_CHANNEL_ID = "download_channel";

    public static final int JOB_REPEAT_TIME = 5;
}
