package llc.chinesepod;

import android.app.Application;
import android.content.SharedPreferences;
import android.util.ArrayMap;
import android.util.Log;

import java.util.ArrayList;

import llc.chinesepod.Utils.ScheduleUtil;

import static llc.chinesepod.Config.JOB_REPEAT_TIME;

public class ChinesePodApplication extends Application {

    public ArrayList<String> imageList;
    public String currentUrl;
    public boolean isRecycle;
    public Float speed_value;

    public boolean auto_download = true;
    public boolean wifi_download = false;

    public boolean manual_download = false;

    @Override
    public void onCreate() {
        super.onCreate();

        SharedPreferences specialRefer = getSharedPreferences("special", MODE_PRIVATE);

        auto_download = specialRefer.getBoolean("auto_download", true);
        wifi_download = specialRefer.getBoolean("wifi_download", false);

        ScheduleUtil.scheduleJob(this, JOB_REPEAT_TIME);
    }
}
