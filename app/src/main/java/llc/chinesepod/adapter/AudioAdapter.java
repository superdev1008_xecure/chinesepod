package llc.chinesepod.adapter;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.ArrayMap;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;

import java.util.ArrayList;

import llc.chinesepod.Config;
import llc.chinesepod.recap.R;

public class AudioAdapter extends BaseAdapter {

    ArrayMap<Integer, String> audioes;
    Activity context;
    Handler handler;

    public AudioAdapter(ArrayMap<Integer, String> audiolist, Activity cnt, Handler hnd) {
        super();
        audioes = audiolist;
        context = cnt;
        handler = hnd;
    }

    @Override
    public int getCount() {
        return audioes.size();
    }

    @Override
    public String getItem(int i) {
        return audioes.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        view = context.getLayoutInflater().inflate(R.layout.list_item, null);
        Button btn = view.findViewById(R.id.btn);

        String btn_text = "";
        if (getItem(i).substring(Config.BASE_AUDIO_URL.length()).contains("3.mp3") ||
                getItem(i).substring(Config.BASE_AUDIO_URL.length()).contains("3.wav")){
            btn_text = "3 MINUTES";
        }else if(getItem(i).substring(Config.BASE_AUDIO_URL.length()).contains("6.mp3") ||
                getItem(i).substring(Config.BASE_AUDIO_URL.length()).contains("6.wav")){
            btn_text = "6 MINUTES";
        }else if(getItem(i).substring(Config.BASE_AUDIO_URL.length()).contains("90.mp3") ||
                getItem(i).substring(Config.BASE_AUDIO_URL.length()).contains("90.wav")){
            btn_text = "90 SECONDS";
        }

        btn.setText(btn_text);

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Message msg = new Message();
                Bundle bundle = new Bundle();
                bundle.putString("id", audioes.get(i));
                msg.setData(bundle);
                handler.sendMessage(msg);
            }
        });

        return view;
    }
}