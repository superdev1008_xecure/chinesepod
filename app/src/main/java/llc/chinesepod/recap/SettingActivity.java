package llc.chinesepod.recap;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Set;

import llc.chinesepod.ChinesePodApplication;

public class SettingActivity extends AppCompatActivity implements CompoundButton.OnCheckedChangeListener, View.OnClickListener {
    private TextView email, version;
    CheckBox auto_download, wifi_download;
    Button disconnect, ok, feedback;
    SharedPreferences specialRefer, audioCheckPrf, audio_info, userRefer;
    public String lessonTitle = null,
            emailAddress = null,
            charSet = null,
            subscription = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);

        email = findViewById(R.id.email);
        version = findViewById(R.id.version);

        auto_download = findViewById(R.id.auto_download);
        auto_download.setOnCheckedChangeListener(this);
        wifi_download = findViewById(R.id.wifi_download);
        wifi_download.setOnCheckedChangeListener(this);

        disconnect = findViewById(R.id.disconnect);
        disconnect.setOnClickListener(this);
        ok = findViewById(R.id.ok);
        ok.setOnClickListener(this);
        feedback = findViewById(R.id.feedback);
        feedback.setOnClickListener(this);

        specialRefer = getSharedPreferences("special", MODE_PRIVATE);
        audioCheckPrf = getSharedPreferences("audioCheckPrf", MODE_PRIVATE);
        audio_info = getSharedPreferences("audio_info", MODE_PRIVATE);
        userRefer = getSharedPreferences("user_info", MODE_PRIVATE);

        ChinesePodApplication app = (ChinesePodApplication) getApplication();
        auto_download.setChecked(app.auto_download);
        wifi_download.setChecked(app.wifi_download);

        init();
    }

    @Override
    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
        ChinesePodApplication app = (ChinesePodApplication) getApplication();
        SharedPreferences.Editor editor = specialRefer.edit();

        int id = compoundButton.getId();
        switch (id){
            case R.id.auto_download:
                app.auto_download = b;
                editor.putBoolean("auto_download", b);
                editor.commit();
                break;
            case R.id.wifi_download:
                app.wifi_download = b;
                editor.putBoolean("wifi_download", b);
                editor.commit();
                break;
        }
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id){
            case R.id.disconnect:
                showDisconnectDialog();
                break;
            case R.id.ok:
                finish();
                break;
            case R.id.feedback:
                Toast.makeText(this, "Coming soon", Toast.LENGTH_SHORT).show();
                break;
        }
    }

    private void showDisconnectDialog() {
        // create an alert builder
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.menu_log_out);

        builder.setMessage(R.string.disconnect_dialog_message);
        // add a button
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                SharedPreferences.Editor editSpecial = specialRefer.edit();
                editSpecial.clear();
                editSpecial.commit();

                SharedPreferences.Editor editCheck = audioCheckPrf.edit();
                editCheck.clear();
                editCheck.commit();

                SharedPreferences.Editor editAudio_info = audio_info.edit();
                editAudio_info.clear();
                editAudio_info.commit();

                SharedPreferences.Editor editUserRefer = userRefer.edit();
                editUserRefer.clear();
                editUserRefer.commit();

                ChinesePodApplication app = (ChinesePodApplication) getApplication();
                app.auto_download = true;
                app.wifi_download = false;

                try{
                    MainActivity.instance.finish();
                }catch (Exception e){
                    e.printStackTrace();
                }
                finish();

                startActivity(new Intent(SettingActivity.this, SplashActivity.class));
            }
        });
        // create and show the alert dialog
        AlertDialog dialog = builder.create();
        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialogInterface) {
                dialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.colorE));
            }
        });

        dialog.show();
    }

    public void init() {
        lessonTitle = specialRefer.getString("lessonTitle", null);
        emailAddress = specialRefer.getString("emailAddress", null);
        charSet = specialRefer.getString("charSet", null);
        subscription = specialRefer.getString("subscription", null);

        email.setText(emailAddress);
        version.setText("version: " + BuildConfig.VERSION_NAME + " (" + BuildConfig.VERSION_CODE + ")");
    }
}
