package llc.chinesepod.recap;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.MenuItem;
import android.view.View;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;

import com.crashlytics.android.Crashlytics;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.navigation.NavigationView;

import java.util.ArrayList;
import java.util.List;

import io.fabric.sdk.android.Fabric;
import llc.chinesepod.ChinesePodApplication;
import llc.chinesepod.Utils.MySwipeableViewPager;
import llc.chinesepod.Utils.ScheduleUtil;
import llc.chinesepod.adapter.FragmentViewPagerAdapter;
import llc.chinesepod.fm.FlashFragment;
import llc.chinesepod.fm.MainFragment;
import llc.chinesepod.fm.TestFragment;
import llc.chinesepod.service.AudioPlayerService;
import llc.chinesepod.service.DownloadService;

import static llc.chinesepod.Config.JOB_REPEAT_TIME;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    public static MainActivity instance;
    public static MySwipeableViewPager viewPager;
    private List<Fragment> fragmentList;

    private ImageView hamburger;

    SharedPreferences specialRefer, audioCheckPrf, audio_info, userRefer;

    public static final int HOME = 0;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    viewPager.setCurrentItem(0);

                    return true;
                case R.id.navigation_card:
                    viewPager.setCurrentItem(1);

                    return true;
            }
            return false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (AudioPlayerService.instance != null){
            startActivity(new Intent(this, ExoPlayerActivity.class));
        }

        try{
            instance.finish();
        }catch (Exception e){
            e.printStackTrace();
        }

        Fabric.with(this, new Crashlytics());

        setContentView(R.layout.activity_main);

        specialRefer = getSharedPreferences("special", MODE_PRIVATE);
        audioCheckPrf = getSharedPreferences("audioCheckPrf", MODE_PRIVATE);
        audio_info = getSharedPreferences("audio_info", MODE_PRIVATE);
        userRefer = getSharedPreferences("user_info", MODE_PRIVATE);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        hamburger = toolbar.findViewById(R.id.hamburger);
        hamburger.setOnClickListener(this);

        boolean save = specialRefer.getBoolean("save", false);
        if (!save){
            hamburger.setVisibility(View.GONE);
        }

        BottomNavigationView navView = findViewById(R.id.bottom_nav_view);
        navView.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        setLogo();

        initPageViewer();

        instance = this;
    }

    private void setLogo(){
        getSupportActionBar().setDisplayShowTitleEnabled(false);
    }

    private void initPageViewer() {
        viewPager= (MySwipeableViewPager) findViewById(R.id.container_fm);
        viewPager.enableSwip(false);

        fragmentList=new ArrayList<Fragment>();
        fragmentList.add(new MainFragment());
        fragmentList.add(new FlashFragment());
        fragmentList.add(new TestFragment());

        FragmentViewPagerAdapter adapter = new FragmentViewPagerAdapter(getSupportFragmentManager(), fragmentList);
        viewPager.setAdapter(adapter);
        adapter.notifyDataSetChanged();
        viewPager.setCurrentItem(0);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    public void selectFragment(int id) {
        viewPager.setCurrentItem(id);
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id){
            case R.id.hamburger:
                Intent intent = new Intent(this, SettingActivity.class);
                startActivity(intent);
                break;
        }
    }

    public Handler showHamburger = new Handler(){
        @Override
        public void handleMessage(@NonNull Message msg) {
            hamburger.setVisibility(View.VISIBLE);
        }
    };
}