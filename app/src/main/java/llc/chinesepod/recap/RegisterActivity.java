package llc.chinesepod.recap;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.viewpager.widget.ViewPager;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.ArrayMap;
import android.util.Log;

import com.google.android.material.tabs.TabLayout;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import llc.chinesepod.Config;
import llc.chinesepod.fm.CheckCodeFragment;
import llc.chinesepod.fm.LoginFragment;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.HttpUrl;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class RegisterActivity extends AppCompatActivity {
    private String m_code;

    ViewPager viewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        setLogo();

        checkCode();

        PagerAdapter adapter = new PagerAdapter(getSupportFragmentManager());
        viewPager = findViewById(R.id.pager);
        viewPager.setAdapter(adapter);

        TabLayout tabLayout = findViewById(R.id.tab_layout);
        tabLayout.setupWithViewPager(viewPager);
    }

    private void checkCode() {
        Intent intent = getIntent();
        m_code = intent.getStringExtra("code");

        OkHttpClient client = new OkHttpClient();

        JSONObject postdata = new JSONObject();
        try {
            postdata.put("client_id", "chinesepodrecap");
            postdata.put("code", m_code);
        } catch(JSONException e){
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        MediaType MEDIA_TYPE = MediaType.parse("application/x-www-form-urlencoded");
        RequestBody body = RequestBody.create(MEDIA_TYPE, postdata.toString());

        Request request = new Request.Builder()
                .url(Config.CHECK_CODE_URL)
                .post(body)
                .header("Content-Type", "application/x-www-form-urlencoded")
                .build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                String mMessage = e.getMessage().toString();
                Log.w("failure Response", mMessage);
                //call.cancel();
                mHandler.sendEmptyMessageDelayed(0, 750);
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {

                String jsonData = response.body().string();
                try {
                    JSONObject obj = new JSONObject(jsonData);
                    int success = obj.getInt("success");
                    if (success == 1){
                        String session_id = obj.getJSONObject("data").getString("sessionid");

                        checkSessionId(session_id);
                    } else {
                        mHandler.sendEmptyMessageDelayed(0, 750);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                response.close();
            }
        });
    }

    private void checkSessionId(String sessionId){
        HttpUrl.Builder urlBuilder
                = HttpUrl.parse(Config.CHECK_SESSION_ID).newBuilder();
        urlBuilder.addQueryParameter("sessionid", sessionId);

        String url = urlBuilder.build().toString();

        Request request = new Request.Builder()
                .url(url)
                .build();

        OkHttpClient client = new OkHttpClient();
        Call call = client.newCall(request);
        client.newCall(request).enqueue(new okhttp3.Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                String mMessage = e.getMessage().toString();
                Log.w("failure Response", mMessage);
                //call.cancel();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {

                String jsonData = response.body().string();
                try {
                    JSONObject obj = new JSONObject(jsonData);
                    int success = obj.getInt("success");
                    if (success == 1){
                        SharedPreferences sharedPreferences = getSharedPreferences("user_info", MODE_PRIVATE);
                        SharedPreferences.Editor editor = sharedPreferences.edit();
                        editor.putString("sessionid", sessionId);
                        editor.commit();

                        startActivity(new Intent(RegisterActivity.this, MainActivity.class));
                        finish();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                response.close();
            }
        });
    }

    private void setLogo(){
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setLogo(R.drawable.logo_s);
        getSupportActionBar().setDisplayUseLogoEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
    }

    private Handler mHandler = new Handler(){
        @Override
        public void handleMessage(@NonNull Message msg) {
           checkCode();
        }
    };

    class PagerAdapter extends FragmentStatePagerAdapter {
        ArrayMap<String, Fragment> _mapFM;

        public PagerAdapter(FragmentManager fm) {
            super(fm);

            _mapFM = new ArrayMap<>();
            _mapFM.put("LOGIN WITH CODE", new CheckCodeFragment());
            _mapFM.put("LOGIN WITH INFO", new LoginFragment());
        }

        @Override
        public Fragment getItem(int position) {
            return _mapFM.valueAt(position);
        }

        @Override
        public int getCount() {
            return _mapFM.size();
        }

        @Nullable
        @Override
        public CharSequence getPageTitle(int position) {
            return _mapFM.keyAt(position);
        }
    }
}
