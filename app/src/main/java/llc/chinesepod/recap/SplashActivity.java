package llc.chinesepod.recap;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;

import com.victor.loading.rotate.RotateLoading;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import llc.chinesepod.Config;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.HttpUrl;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class SplashActivity extends AppCompatActivity {

    TextView no_internet;
    RotateLoading loading;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SharedPreferences specialRefer = getSharedPreferences("special", MODE_PRIVATE);
        if (specialRefer.getBoolean("save", false)){
            new Handler(){
                @Override
                public void handleMessage(@NonNull Message msg) {
                    startActivity(new Intent(SplashActivity.this, MainActivity.class));
                    finish();
                }
            }.sendEmptyMessage(0);
            return;
//            no_internet.setVisibility(View.GONE);
        }

        setContentView(R.layout.activity_splash);

        loading = findViewById(R.id.progress_circular);
        loading.start();

        SharedPreferences sharedPreferences = getPreferences(MODE_PRIVATE);
        String code = sharedPreferences.getString("code", null);

        no_internet = findViewById(R.id.no_internet);

        connectApi.sendEmptyMessage(0);
    }

    private  Handler connectApi = new Handler(){
        @Override
        public void handleMessage(@NonNull Message msg) {
            SharedPreferences userRefer = getSharedPreferences("user_info", MODE_PRIVATE);
            String sessionId = userRefer.getString("sessionid", null);
            SharedPreferences specialRefer = getSharedPreferences("special", MODE_PRIVATE);
            if(!specialRefer.getBoolean("save", false) && !isNetworkAvailable()){
                no_internet.setVisibility(View.VISIBLE);
                connectApi.sendEmptyMessageDelayed(0, 3000);
            }else if(sessionId != null){
                checkSessionId(sessionId);
            }else{
                requestCode();
            }
        }
    };

    private void checkCode(String code) {
        OkHttpClient client = new OkHttpClient();

        JSONObject postdata = new JSONObject();
        try {
            postdata.put("client_id", "chinesepodrecap");
            postdata.put("code", code);
        } catch(JSONException e){
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        MediaType MEDIA_TYPE = MediaType.parse("application/x-www-form-urlencoded");
        RequestBody body = RequestBody.create(MEDIA_TYPE, postdata.toString());

        Request request = new Request.Builder()
                .url(Config.CHECK_CODE_URL)
                .post(body)
                .header("Content-Type", "application/x-www-form-urlencoded")
                .build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                String mMessage = e.getMessage().toString();
                Log.w("failure Response", mMessage);
                //call.cancel();
                requestCode();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {

                String jsonData = response.body().string();
                try {
                    JSONObject obj = new JSONObject(jsonData);
                    int success = obj.getInt("success");
                    if (success == 1){
                        SharedPreferences sharedPreferences = getPreferences(MODE_PRIVATE);
                        SharedPreferences.Editor editor = sharedPreferences.edit();

                        String session_id = obj.getJSONObject("data").getString("session_id");
                        editor.putString("session_id", session_id);
                        editor.commit();

                        startActivity(new Intent(SplashActivity.this, MainActivity.class));
                        finish();
                    } else {
                        requestCode();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    requestCode();
                }
                response.close();
            }
        });
    }

    private void requestCode() {
        OkHttpClient client = new OkHttpClient();

        JSONObject postdata = new JSONObject();
        try {
            postdata.put("client_id", "chinesepodrecap");
        } catch(JSONException e){
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        MediaType MEDIA_TYPE = MediaType.parse("application/x-www-form-urlencoded");
        RequestBody body = RequestBody.create(MEDIA_TYPE, postdata.toString());

        Request request = new Request.Builder()
                .url(Config.GET_CODE_URL)
                .post(body)
                .header("Content-Type", "application/x-www-form-urlencoded")
                .build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                String mMessage = e.getMessage().toString();
                Log.w("failure Response", mMessage);
                requestCode();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {

                String jsonData = response.body().string();
                try {
                    JSONObject obj = new JSONObject(jsonData);
                    int success = obj.getInt("success");
                    if (success == 1){
                        String code = obj.getJSONObject("data").getString("code");
                        //store in local storage
                        SharedPreferences sharedPreferences = getPreferences(MODE_PRIVATE);
                        SharedPreferences.Editor editor = sharedPreferences.edit();
                        editor.putString("code", code);
                        editor.commit();
                        //start new activity
                        Intent intent = new Intent(new Intent(SplashActivity.this, IntroActivity.class));
                        intent.putExtra("code", code);
                        startActivity(intent);
                        finish();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                response.close();
            }
        });
    }
    private void checkSessionId(String sessionId){
        HttpUrl.Builder urlBuilder
                = HttpUrl.parse(Config.CHECK_SESSION_ID).newBuilder();
        urlBuilder.addQueryParameter("sessionid", sessionId);

        String url = urlBuilder.build().toString();

        Request request = new Request.Builder()
                .url(url)
                .build();

        OkHttpClient client = new OkHttpClient();
        Call call = client.newCall(request);
        client.newCall(request).enqueue(new okhttp3.Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                String mMessage = e.getMessage().toString();
                Log.w("failure Response", mMessage);

                requestCode();
                //call.cancel();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {

                String jsonData = response.body().string();
                try {
                    JSONObject obj = new JSONObject(jsonData);
                    int success = obj.getInt("success");
                    if (success == 1){
                        SharedPreferences sharedPreferences = getSharedPreferences("user_info", MODE_PRIVATE);
                        SharedPreferences.Editor editor = sharedPreferences.edit();
                        editor.putString("sessionid", sessionId);
                        editor.commit();

                        startActivity(new Intent(SplashActivity.this, MainActivity.class));
                        finish();
                    }else{
                        requestCode();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    requestCode();
                }
                response.close();
            }
        });
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager manager =
                (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = manager.getActiveNetworkInfo();
        boolean isAvailable = false;
        if (networkInfo != null && networkInfo.isConnected()) {
            // Network is present and connected
            isAvailable = true;
        }
        return isAvailable;
    }
}
