package llc.chinesepod.recap;

import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.ArrayMap;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;

import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.PlaybackParameters;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.source.ConcatenatingMediaSource;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelectionArray;
import com.google.android.exoplayer2.ui.PlayerView;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.upstream.cache.CacheDataSource;
import com.google.android.exoplayer2.upstream.cache.CacheDataSourceFactory;
import com.google.android.exoplayer2.util.Util;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.smarteist.autoimageslider.SliderAnimations;
import com.smarteist.autoimageslider.SliderView;
import com.victor.loading.rotate.RotateLoading;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Map;

import butterknife.ButterKnife;
import llc.chinesepod.ChinesePodApplication;
import llc.chinesepod.Utils.DownloadUtil;
import llc.chinesepod.Utils.ScheduleUtil;
import llc.chinesepod.adapter.SliderAdapter;
import llc.chinesepod.service.AudioPlayerService;
import llc.chinesepod.service.DownloadService;
import me.zhanghai.android.materialprogressbar.MaterialProgressBar;

public class ExoPlayerActivity extends AppCompatActivity implements Player.EventListener, View.OnClickListener {
    private static final String TAG = "ExoPlayerActivity";

    private static final String KEY_VIDEO_URI = "id";


    PlayerView videoFullScreenPlayer;
    RotateLoading spinnerVideoDetails;

//  View controllers;
    public ImageView play, pause, forward, backward, preload, blur, down_arrow;
    public LinearLayout play_min, pause_min;
    SeekBar seekBar;
    MaterialProgressBar progress_number;

    String mediaUri;
    public SimpleExoPlayer player;
    Handler mHandler;
    Runnable mRunnable;

    SliderView sliderView;
    SliderAdapter adapter;

    TextView speed_label, left_time, current_time, total_title;
    View speed, recycle_on, recycle_off,recycle;
    RelativeLayout heading_controller, bottomSheetLayout;

    SharedPreferences specialRefer;

    boolean isRecycle = false;

    ArrayList<Float> arraySpeed;
    private Float speed_value;

    RelativeLayout min_controller;

    public static ExoPlayerActivity instance = null;
    private BottomSheetBehavior bottomSheetBehavior;
    private long hideDelayMin = 6000;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        try{
            instance.finish();
        }catch (Exception e){
            e.printStackTrace();
        }
        instance = this;

        setContentView(R.layout.activity_exo_player);

        ButterKnife.bind(this);
        specialRefer = getSharedPreferences("special", MODE_PRIVATE);
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);

        AudioPlayerService service = AudioPlayerService.getInstance();
        if (service != null){
            service.stopSelf();
        }

        ChinesePodApplication app = (ChinesePodApplication)getApplication();
        mediaUri = app.currentUrl;

        speed = findViewById(R.id.speed);
        speed.setOnClickListener(this);
        speed_label = findViewById(R.id.speed_label);
        recycle_on = findViewById(R.id.recycle_on);
        recycle_off = findViewById(R.id.recycle_off);
        recycle = findViewById(R.id.recycle);
        recycle.setOnClickListener(this);
        blur = findViewById(R.id.blur);
        blur.setOnClickListener(this);
        play = findViewById(R.id.play);
        play.setOnClickListener(this);
        pause = findViewById(R.id.pause);
        pause.setOnClickListener(this);
        play_min = findViewById(R.id.play_min);
        play_min.setOnClickListener(this);
        pause_min = findViewById(R.id.pause_min);
        pause_min.setOnClickListener(this);
        forward = findViewById(R.id.forward);
        forward.setOnClickListener(this);
        backward = findViewById(R.id.backward);
        backward.setOnClickListener(this);
        seekBar = findViewById(R.id.seekBar);
        progress_number = findViewById(R.id.progress_number);
        heading_controller = findViewById(R.id.heading_controller);
        down_arrow = findViewById(R.id.down_arrow);
        left_time = findViewById(R.id.left_time);

        current_time = findViewById(R.id.current_time);
        total_title = findViewById(R.id.total_title);

        videoFullScreenPlayer = findViewById(R.id.videoFullScreenPlayer);
        spinnerVideoDetails = findViewById(R.id.spinnerVideoDetails);

        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                player.seekTo(seekBar.getProgress() * player.getDuration() / 100);
                bottomSheetLayout.removeCallbacks(hideBottomSheet);
                bottomSheetLayout.postDelayed(hideBottomSheet,hideDelayMin);
            }
        });

        mGetSeeking.sendEmptyMessage(0);

        String CharSet = null;
        try {
            CharSet = specialRefer.getString("charSet", null).compareTo("simplified") == 0? "s" : "t";
        }catch (Exception e){
            e.printStackTrace();
        }

        ArrayMap<Integer, String> imgList = new ArrayMap<>();
        if (CharSet != null){
            int endIndex = 0;
            if (app.currentUrl != null && app.currentUrl.contains(".mp3"))
                endIndex = app.currentUrl.indexOf(".mp3");
            else if (app.currentUrl != null && app.currentUrl.contains(".wav"))
                endIndex = app.currentUrl.indexOf(".wav");
            try{
                if(endIndex > 0){
                    for (String img: app.imageList){
                        if (img.contains(app.currentUrl.substring(0, endIndex))){
                            if (!img.contains("." + CharSet + "."))
                                continue;
                            String strKey = img.substring(app.currentUrl.substring(0, endIndex).length() + 1, img.indexOf("." + CharSet));
                            Integer key = (Integer.valueOf(strKey) / 100) * 60 + Integer.valueOf(strKey) % 100;
                            imgList.put(key, img);
                        }
                    }
                }
            }catch (Exception e){
                e.printStackTrace();
            }
        }

        sliderView = findViewById(R.id.imageSlider);

        if (imgList != null && imgList.size() > 0){
            adapter = new SliderAdapter(this, imgList, new Handler(){
                @Override
                public void handleMessage(@NonNull Message msg) {
                }
            });

            sliderView.setSliderAdapter(adapter);

            preload = findViewById(R.id.preload);
            preload.setVisibility(View.GONE);
        }

        sliderView.setSliderTransformAnimation(SliderAnimations.FADETRANSFORMATION);

        arraySpeed = new ArrayList<>();
        arraySpeed.add(new Float(1));
        arraySpeed.add(new Float(1.25));
        arraySpeed.add(new Float(1.5));
        arraySpeed.add(new Float(0.25));
        arraySpeed.add(new Float(0.5));
        arraySpeed.add(new Float(0.75));

        setUp();

        SharedPreferences sharedPreferences = getApplication().getSharedPreferences("audio_info", MODE_PRIVATE);

        long seek = sharedPreferences.getLong(mediaUri, 0);

        player.seekTo(seek);

        if (app.isRecycle){
            recycle_on.setVisibility(View.VISIBLE);
            recycle_off.setVisibility(View.GONE);
        }else {
            recycle_on.setVisibility(View.GONE);
            recycle_off.setVisibility(View.VISIBLE);
        }
        isRecycle = app.isRecycle;

        speed_value = app.speed_value;
        if(speed_value != null){
            PlaybackParameters param = new PlaybackParameters(speed_value);
            player.setPlaybackParameters(param);

            speed_label.setText(speed_value.toString() + "x");
        }

        bottomSheetLayout = findViewById(R.id.bottomSheetLayout);
        bottomSheetLayout.setOnClickListener(this);
        bottomSheetBehavior = BottomSheetBehavior.from(bottomSheetLayout);
        bottomSheetBehavior.setHideable(false);
        bottomSheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View view, int newState) {
                // Check Logs to see how bottom sheets behaves
                switch (newState) {
                    case BottomSheetBehavior.STATE_COLLAPSED:
                        heading_controller.setVisibility(View.VISIBLE);
                        down_arrow.setVisibility(View.GONE);
                        progress_number.setVisibility(View.VISIBLE);
                        left_time.setVisibility(View.VISIBLE);
                        break;
                    case BottomSheetBehavior.STATE_DRAGGING:
                    case BottomSheetBehavior.STATE_EXPANDED:
                        heading_controller.setVisibility(View.GONE);
                        down_arrow.setVisibility(View.VISIBLE);
                        progress_number.setVisibility(View.GONE);
                        left_time.setVisibility(View.GONE);
                        bottomSheetLayout.removeCallbacks(hideBottomSheet);
                        bottomSheetLayout.postDelayed(hideBottomSheet,hideDelayMin);
                        break;
                    case BottomSheetBehavior.STATE_HIDDEN:
                        Log.e("Bottom Sheet Behaviour", "STATE_HIDDEN");
                        break;
                    case BottomSheetBehavior.STATE_SETTLING:
                        Log.e("Bottom Sheet Behaviour", "STATE_SETTLING");
                        break;
                }
            }

            @Override
            public void onSlide(@NonNull View view, float v) {
                blur.setAlpha(v * 2 / 3);
            }
        });

        min_controller = findViewById(R.id.min_controller);
        min_controller.setOnClickListener(this);

        initPlayerController();
    }

    private void setUp() {
        initializePlayer();
        if (mediaUri == null) {
            return;
        }
        buildMediaSource(Uri.parse(mediaUri));
    }

    private void initializePlayer() {
        if (player == null) {
            player = ExoPlayerFactory.newSimpleInstance(this, new DefaultTrackSelector());

            videoFullScreenPlayer.setPlayer(player);
        }


    }

    private void buildMediaSource(Uri mUri) {

        DefaultDataSourceFactory dataSourceFactory = new DefaultDataSourceFactory(
                this, Util.getUserAgent(this, getString(R.string.app_name)));

        ConcatenatingMediaSource concatenatingMediaSource = new ConcatenatingMediaSource();

        CacheDataSourceFactory cacheDataSourceFactory = new CacheDataSourceFactory(
                DownloadUtil.getCache(this),
                dataSourceFactory,
                CacheDataSource.FLAG_IGNORE_CACHE_ON_ERROR);

        MediaSource mediaSource = new ExtractorMediaSource.Factory(cacheDataSourceFactory)
                .createMediaSource(mUri);
        concatenatingMediaSource.addMediaSource(mediaSource);

        player.prepare(mediaSource);
        player.setPlayWhenReady(true);
        player.addListener(this);
    }

    public void releasePlayer() {
        if (player != null) {
            player.release();
            player = null;
        }
    }

    private void pausePlayer() {
        if (player != null) {
            player.setPlayWhenReady(false);
            player.getPlaybackState();
        }
    }

    private void resumePlayer() {
        if (player != null) {
            player.setPlayWhenReady(true);
            player.getPlaybackState();
        }
    }

    @Override
    public void onPause() {
        super.onPause();

        pausePlayer();
        if (mRunnable != null) {
            mHandler.removeCallbacks(mRunnable);
        }

        SharedPreferences sharedPreferences = getApplication().getSharedPreferences("audio_info", MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();

        editor.putLong(mediaUri, player.getContentPosition());
        editor.putString("title", "ChinesePod");
        editor.putString("description", "Recap audio");

        editor.commit();

        Intent intent = new Intent(this, AudioPlayerService.class);

        if ( play.getVisibility() == View.GONE && play_min.getVisibility() == View.GONE){
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                startForegroundService(intent);
            }else {
                startService(intent);
            }
//            spinnerVideoDetails.start();
        }else{
            //clear internal storage
            String dir_name = specialRefer.getString("lesson_directory", null);
            if(dir_name == null || DownloadService.instance != null){
                return;
            }

            File directory = getFilesDir();
            File[] files = directory.listFiles();
            for (File audio_dir: files){
                if(audio_dir.getName().compareTo(dir_name) == 0 || audio_dir.getName().compareTo(".Fabric") == 0){
                    continue;
                }
                if (audio_dir.isDirectory())
                {
                    String[] children = audio_dir.list();
                    for (int i = 0; i < children.length; i++)
                    {
                        new File(audio_dir, children[i]).delete();
                    }
                }
                audio_dir.delete();
            }
        }
    }

    private void initPlayerController() {
        if (player.getPlayWhenReady() == true){
            play.setVisibility(View.GONE);
            play_min.setVisibility(View.GONE);
            pause.setVisibility(View.VISIBLE);
            pause_min.setVisibility(View.VISIBLE);
        }else {
            play.setVisibility(View.VISIBLE);
            play_min.setVisibility(View.VISIBLE);
            pause.setVisibility(View.GONE);
            pause_min.setVisibility(View.GONE);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        resumePlayer();
        AudioPlayerService service = AudioPlayerService.getInstance();
        if (service != null){
            service.stopSelf();
        }
        initPlayerController();
    }

    @Override
    public void onDestroy() {
        releasePlayer();
        AudioPlayerService service = AudioPlayerService.getInstance();
        if (service != null){
            service.stopSelf();
        }

        DownloadService downloadService = DownloadService.instance;
        if (downloadService != null){
            downloadService.stopSelf();
        }

        super.onDestroy();
    }

    @Override
    public void onTimelineChanged(Timeline timeline, Object manifest, int reason) {

    }

    @Override
    public void onTracksChanged(TrackGroupArray trackGroups, TrackSelectionArray trackSelections) {

    }

    @Override
    public void onLoadingChanged(boolean isLoading) {

    }

    @Override
    public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
        switch (playbackState) {

            case Player.STATE_BUFFERING:
//                spinnerVideoDetails.start();
                break;
            case Player.STATE_ENDED:
                // Activate the force enable
                break;
            case Player.STATE_IDLE:

                break;
            case Player.STATE_READY:
//                spinnerVideoDetails.stop();

                break;
            default:
                // status = PlaybackStatus.IDLE;
                break;
        }
    }

    @Override
    public void onRepeatModeChanged(int repeatMode) {

    }

    @Override
    public void onShuffleModeEnabledChanged(boolean shuffleModeEnabled) {

    }

    @Override
    public void onPlayerError(ExoPlaybackException error) {

    }

    @Override
    public void onPositionDiscontinuity(int reason) {

    }

    @Override
    public void onPlaybackParametersChanged(PlaybackParameters playbackParameters) {

    }

    @Override
    public void onSeekProcessed() {
    }

    private Handler mGetSeeking = new Handler(){
        @Override
        public void handleMessage(@NonNull Message msg) {
            super.handleMessage(msg);

            if (player != null){
                long max = player.getDuration();
                long current = player.getCurrentPosition();

                Integer second = (int)(current / 1000);
                if(adapter != null && adapter.getContent() != null){
                    Message message = new Message();
                    message.arg1 = second;
                    selectSlide.sendMessage(message);
                }
                if(isRecycle && current > max - 2000){
                    player.seekTo(0);
                }
                seekBar.setProgress((int)(100 * player.getCurrentPosition() / player.getDuration()));
                progress_number.setProgress((int)(100 * player.getCurrentPosition() / player.getDuration()));

                Integer left_min = (int)(((max-current) / 1000) / 60);
                Integer left_sec = (int)(((max-current) / 1000) % 60);
                String leftTime = new String();
                if (left_min > 0){
                    leftTime += left_min.toString() + " min ";
                }
                if (left_sec > 0){
                   leftTime += left_sec.toString() + " sec ";
                } else{
                    leftTime += "0 sec ";
                }
                leftTime += "left";
                left_time.setText(leftTime);

                Integer crtMin = (int)(((current) / 1000) / 60);
                Integer crtSec = (int)(((current) / 1000) % 60);
                Integer tMin = (int)(((max) / 1000) / 60);
                Integer tSec = (int)(((max) / 1000) % 60);

                if (current > max){
                    crtSec = tSec;
                    crtMin = tMin;
                }

                String strCrtMin = "00", strCrtSec = "00", strTmin = "00", strTSec = "00";
                if (crtMin < 10){
                    strCrtMin = "0" + crtMin.toString();
                }else {
                    strCrtMin = crtMin.toString();
                }
                if (crtSec < 10) {
                    strCrtSec = "0" + crtSec.toString();
                }else {
                    strCrtSec = crtSec.toString();
                }
                if (tMin < 10) {
                    strTmin = "0" + tMin.toString();
                }else {
                    strTmin = tMin.toString();
                }
                if (tSec < 10) {
                    strTSec = "0" + tSec.toString();
                }else {
                    strTSec = tSec.toString();
                }
                if (crtMin >= 0 && crtSec >= 0 && tMin >= 0 && tSec >= 0){
                    String scrTime = strCrtMin + ":" + strCrtSec;
                    String tTime = strTmin + ":" + strTSec;

                    current_time.setText(scrTime);
                    total_title.setText(tTime);
                }
            }
            mGetSeeking.sendEmptyMessageDelayed(0, 10);
        }
    };

    Handler selectSlide = new Handler(){
        @Override
        public void handleMessage(@NonNull Message msg) {
            Integer pos = msg.arg1;
            int index = -1;
            ArrayList<Integer> arrayKey = new ArrayList<>();
            for (Map.Entry<Integer, String> entry : adapter.getContent().entrySet()){
                arrayKey.add(entry.getKey());
            }
            Collections.sort(arrayKey);
            for (Integer i : arrayKey){
                if (i <= pos){
                    index = i;
                } else{
                    break;
                }
            }
            if(index > 0){
                preload.setVisibility(View.GONE);
                int position = 0;
                for (Map.Entry<Integer, String> entry : adapter.getContent().entrySet()){
                    if (index == entry.getKey()){
                        break;
                    }
                    position++;
                }
                sliderView.setCurrentPagePosition(position);
            } else{
                preload.setVisibility(View.VISIBLE);
            }
        }
    };


    @Override
    public void onClick(View view) {
        ChinesePodApplication app = (ChinesePodApplication)getApplication();
        switch (view.getId()){
            case R.id.play:
            case R.id.play_min:
                play.setVisibility(View.GONE);
                play_min.setVisibility(View.GONE);
                pause.setVisibility(View.VISIBLE);
                pause_min.setVisibility(View.VISIBLE);
                player.setPlayWhenReady(true);
                break;
            case R.id.pause:
            case R.id.pause_min:
                play.setVisibility(View.VISIBLE);
                play_min.setVisibility(View.VISIBLE);
                pause.setVisibility(View.GONE);
                pause_min.setVisibility(View.GONE);
                player.setPlayWhenReady(false);
                break;
            case R.id.forward:
                if(player.getCurrentPosition() + 5000 <= player.getDuration())
                    player.seekTo(player.getCurrentPosition() + 5000);
                break;
            case R.id.backward:
                if (player.getCurrentPosition() - 5000 >= 0)
                player.seekTo(player.getCurrentPosition() - 5000);
                break;
            case R.id.recycle:
                isRecycle = !isRecycle;
                if (isRecycle){
                    recycle_on.setVisibility(View.VISIBLE);
                    recycle_off.setVisibility(View.GONE);
                }else {
                    recycle_on.setVisibility(View.GONE);
                    recycle_off.setVisibility(View.VISIBLE);
                }
                app.isRecycle = isRecycle;
                break;
            case R.id.speed:
                speed_value = player.getPlaybackParameters().speed;
                int index = arraySpeed.indexOf(new Float(speed_value));
                if(index + 1< arraySpeed.size()){
                    speed_value = arraySpeed.get(index + 1);
                }else {
                    speed_value = arraySpeed.get(0);
                }
                PlaybackParameters param = new PlaybackParameters(speed_value);
                player.setPlaybackParameters(param);

                speed_label.setText(speed_value.toString() + "x");

                app.speed_value = speed_value;

                break;
            case R.id.min_controller:
                if (bottomSheetBehavior.getState() == BottomSheetBehavior.STATE_EXPANDED){
                    bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                } else{
                    bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                }
                break;
            case R.id.blur:
                bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                break;
        }
        bottomSheetLayout.removeCallbacks(hideBottomSheet);
        bottomSheetLayout.postDelayed(hideBottomSheet,hideDelayMin);
    }

    Runnable hideBottomSheet = new Runnable() {
        public void run() {
            bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
        }
    };

    @Override
    public void onBackPressed() {
        if ( play.getVisibility() == View.GONE && play_min.getVisibility() == View.GONE){
            play.setVisibility(View.VISIBLE);
            play_min.setVisibility(View.VISIBLE);
            pause.setVisibility(View.GONE);
            pause_min.setVisibility(View.GONE);
            player.setPlayWhenReady(false);
        }else{
            super.onBackPressed();
        }
    }
}