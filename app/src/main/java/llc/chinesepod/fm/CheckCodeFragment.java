package llc.chinesepod.fm;

import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import llc.chinesepod.recap.R;
import llc.chinesepod.recap.RegisterActivity;

public class CheckCodeFragment extends Fragment {

    private String m_code;
    TextView code;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_check_code, container, false);

        Intent intent = getActivity().getIntent();
        m_code = intent.getStringExtra("code");

        code = view.findViewById(R.id.code);
        code.setText(m_code);

        view.findViewById(R.id.copy).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ClipboardManager cm = (ClipboardManager)getActivity().getSystemService(Context.CLIPBOARD_SERVICE);
                cm.setText(code.getText());
                Toast.makeText(getActivity(), "Copied to clipboard", Toast.LENGTH_SHORT).show();
            }
        });

        return view;
    }
}
