package llc.chinesepod.fm;

import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.ArrayMap;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.victor.loading.rotate.RotateLoading;

import java.io.File;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

import llc.chinesepod.ChinesePodApplication;
import llc.chinesepod.Config;
import llc.chinesepod.adapter.AudioAdapter;
import llc.chinesepod.recap.ExoPlayerActivity;
import llc.chinesepod.recap.MainActivity;
import llc.chinesepod.recap.R;

import static android.content.Context.MODE_PRIVATE;

public class MainFragment extends Fragment{

    ListView audio_listview;
    AudioAdapter adapter;

    RotateLoading progress_circular;
    ProgressBar progress_number;

    TextView title, intro, lesson_number;

    LinearLayout no_response, no_recap;

    private String lesson_id;

    ArrayMap<Integer, String> audioList = new ArrayMap<>();
    ArrayList<String> imgList = new ArrayList<>();

    SharedPreferences specialRefer;

    String mainBK;

    View m_view;

    ImageView bk;

    static public MainFragment instance = null;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        instance = this;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        m_view = inflater.inflate(R.layout.fragment_main, container, false);

        bk = m_view.findViewById(R.id.bk);

        audio_listview = m_view.findViewById(R.id.audio_list);
        adapter = new AudioAdapter(audioList, getActivity(), new Handler(){
            @Override
            public void handleMessage(@NonNull Message msg) {
                super.handleMessage(msg);
                Bundle bundle = msg.getData();
                startLesson(bundle.getString("id"));
            }
        });
        audio_listview.setAdapter(adapter);

       progress_circular = m_view.findViewById(R.id.progress_circular);
       progress_number = m_view.findViewById(R.id.progress_number);

        title = m_view.findViewById(R.id.title);
        intro = m_view.findViewById(R.id.intro);
        no_recap = m_view.findViewById(R.id.no_recap);

        lesson_number = m_view.findViewById(R.id.lesson_number);

        no_response = m_view.findViewById(R.id.no_response);

        ((ChinesePodApplication)getActivity().getApplication()).imageList = imgList;

        specialRefer = getActivity().getSharedPreferences("special", MODE_PRIVATE);
        lesson_id = specialRefer.getString("lesson_id", null);

        if (lesson_id == null){
            startDetermain();
            return m_view;
        }

        getInternalAudios();

        setBK();

        return m_view;
    }



    private void getInternalAudios() {
        boolean isRecap = specialRefer.getBoolean("no_recap", false);
        if (isRecap){
            no_recap.setVisibility(View.VISIBLE);
            intro.setVisibility(View.INVISIBLE);
            progress_circular.stop();
            title.setText(null);

            return;

        } else {
            no_recap.setVisibility(View.INVISIBLE);
            intro.setVisibility(View.VISIBLE);
        }

        String dir_name = specialRefer.getString("lesson_directory", null);
        if(dir_name == null){
            return;
        }

        File directory = new File(getActivity().getFilesDir()+ "/" + dir_name);
        if (!directory.exists()){
            title.setText(R.string.no_lesson);

            return;
        }
        File[] files = directory.listFiles();
        audioList.clear();
        imgList.clear();
        for (File audio: files){
            String path = Uri.fromFile(audio).toString();
            if(path.contains(".mp3") || path.contains(".wav")) {
                String url = Uri.fromFile(audio).toString();
                if (url.substring(Config.BASE_AUDIO_URL.length()).contains("3.mp3") ||
                        url.substring(Config.BASE_AUDIO_URL.length()).contains("3.wav")){
                    audioList.put(1, url);
                }else if(url.substring(Config.BASE_AUDIO_URL.length()).contains("6.mp3") ||
                        url.substring(Config.BASE_AUDIO_URL.length()).contains("6.wav")){
                    audioList.put(2, url);
                }else if(url.substring(Config.BASE_AUDIO_URL.length()).contains("90.mp3") ||
                        url.substring(Config.BASE_AUDIO_URL.length()).contains("90.wav")){
                    audioList.put(0, url);
                }
            } else if(path.contains(".png") || path.contains(".jpg")){
                imgList.add(Uri.fromFile(audio).toString());
            }
        }

        if (audioList.size()==0){
            title.setText(R.string.no_lesson);
        } else {
            intro.setVisibility(View.INVISIBLE);
        }

        adapter.notifyDataSetChanged();
    }

    private void startLesson(String strUri){

//        progress_circular.start();
//        audio_listview.setVisibility(View.GONE);
//        new Handler(){
//            @Override
//            public void handleMessage(@NonNull Message msg) {
//                progress_circular.stop();
//                audio_listview.setVisibility(View.VISIBLE);
//            }
//        }.sendEmptyMessageDelayed(0, 1000);

        Intent intent = new Intent(getActivity(), ExoPlayerActivity.class);

        ((ChinesePodApplication)getActivity().getApplication()).currentUrl = strUri;
        startActivity(intent);
    }

    private void startDetermain(){
        title.setText(null);
        imgList.clear();
        audioList.clear();

        Log.d("recap", "determina");
        progress_circular.start();
        intro.setVisibility(View.VISIBLE);
    }


    public Handler showProgress = new Handler(){
        @Override
        public void handleMessage(@NonNull Message msg) {
            progress_number.setProgress(msg.arg1);
            if (msg.arg1 >= 100){
                progress_number.setVisibility(View.GONE);
            }else {
                progress_number.setVisibility(View.VISIBLE);
            }
        }
    };

    public Handler stopProgress = new Handler(){
        @Override
        public void handleMessage(@NonNull Message msg) {
            progress_circular.stop();
            intro.setVisibility(View.INVISIBLE);
            progress_number.setProgress(0);
            progress_number.setVisibility(View.GONE);
            title.setText(null);
            if (msg.arg1 == 1)
                getInternalAudios();
            else if (audioList == null || audioList.size() < 1 || msg.arg1 == 0){
                try{
                    SharedPreferences audioCheckPrf = getActivity().getSharedPreferences("audioCheckPrf", MODE_PRIVATE);
                    Long savedTime = audioCheckPrf.getLong("time", 0);
                    if (savedTime <= 0)
                        title.setText(R.string.download_failed);
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        }
    };

    public Handler setBackground = new Handler(){
        @Override
        public void handleMessage(@NonNull Message msg) {
            setBK();
        }
    };

    private void setBK() {
        mainBK = specialRefer.getString("mainBK", null);
        if (mainBK != null){
            bk.setImageURI(Uri.parse(mainBK));
        } else {
            bk.setImageBitmap(null);
        }
    }

    public Handler showNotify = new Handler(){
        @Override
        public void handleMessage(@NonNull Message msg) {
            try{
                specialRefer = getActivity().getSharedPreferences("special", MODE_PRIVATE);
                boolean isRecap = specialRefer.getBoolean("no_recap", false);
                if (isRecap){
                    no_recap.setVisibility(View.VISIBLE);
                    intro.setVisibility(View.INVISIBLE);
                    return;

                } else if (!isRecap && audioList.size() <= 0) {
                    no_recap.setVisibility(View.INVISIBLE);
                    intro.setVisibility(View.VISIBLE);
                }
                title.setText(null);
                if ((audioList !=null && audioList.size() > 0) || (mainBK != null && !mainBK.isEmpty())) {
                    if (audioList.size() == 0){
                        title.setText(R.string.no_lesson);
                    }
                    return;
                }
                switch (msg.arg1){
                    case 0:
                        progress_circular.stop();
                        intro.setVisibility(View.INVISIBLE);
                        title.setText(R.string.no_response);
                        no_response.setVisibility(View.VISIBLE);
                        break;
                    case 1:
                        progress_circular.stop();
                        progress_number.setProgress(0);
                        progress_number.setVisibility(View.GONE);
                        title.setText(R.string.connection_failed);
                        no_response.setVisibility(View.GONE);
                        break;
                    case 2:
                        intro.setVisibility(View.VISIBLE);
                        intro.setText(R.string.connecting_intro);
                        progress_circular.start();
                        title.setText(R.string.connecting);
                        no_response.setVisibility(View.GONE);
                        break;
                    case 4:
                        progress_circular.stop();
                        intro.setVisibility(View.INVISIBLE);
                        title.setText(R.string.no_lesson);
                        break;
                    case 5:
                        if (!progress_circular.isStart()){
                            progress_circular.start();
                        }
                        intro.setVisibility(View.VISIBLE);
                        intro.setText(R.string.downloading_intro);
                        Integer progress_val = msg.arg2;
                        title.setText(progress_val.toString() + "%");
                        break;
                    case 6:
                        progress_circular.stop();
                        progress_number.setProgress(0);
                        progress_number.setVisibility(View.GONE);
                        title.setText(R.string.no_internet);
                        break;
                    case 7:
                        progress_circular.stop();
                        intro.setVisibility(View.INVISIBLE);
                        progress_number.setProgress(0);
                        progress_number.setVisibility(View.GONE);
                        title.setText(R.string.no_lesson);
                        break;
                    case 8:
                        progress_circular.stop();
                        progress_number.setProgress(0);
                        progress_number.setVisibility(View.GONE);
                        title.setText(R.string.download_failed);
                        no_response.setVisibility(View.GONE);
                        break;
                }
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    };

    public Handler NoRecapHandle = new Handler(){
        @Override
        public void handleMessage(@NonNull Message msg) {
            if (audioList.size() > 0){
                audioList.clear();
                adapter.notifyDataSetChanged();
            }
            setBK();
            progress_circular.stop();
            title.setText(null);
            no_recap.setVisibility(View.VISIBLE);
            intro.setVisibility(View.INVISIBLE);

            Bundle bundle = msg.getData();
            lesson_number.setText("[Lesson number : " + bundle.getString("lesson_id") +"]");
        }
    };

    @Override
    public void onPause() {
        getInternalAudios();
        setBK();
        super.onPause();
    }

    @Override
    public void onStart() {
        super.onStart();
    }
}
