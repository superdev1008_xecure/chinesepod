package llc.chinesepod.fm;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import android.os.Handler;
import android.os.Message;
import android.os.VibrationEffect;
import android.os.Vibrator;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.TextView;

import com.amazonaws.util.BinaryUtils;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.victor.loading.rotate.RotateLoading;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import llc.chinesepod.Config;
import llc.chinesepod.recap.MainActivity;
import llc.chinesepod.recap.R;
import llc.chinesepod.recap.RegisterActivity;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.HttpUrl;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

import static android.content.Context.MODE_PRIVATE;

public class LoginFragment extends Fragment {

    TextInputLayout email_layout, password_layout;
    TextInputEditText email, password;
    TextView notification;
    Button login;
    RotateLoading progress_circular;

    String client_secret = "e3zhp8wo6emoki2i8jskqxi9l0j84n5j21x2ir9d";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_login, container, false);
        email_layout = view.findViewById(R.id.email_layout);
        password_layout = view.findViewById(R.id.password_layout);
        notification = view.findViewById(R.id.notification);
        email = view.findViewById(R.id.email);
        password = view.findViewById(R.id.password);
        login = view.findViewById(R.id.login);
        progress_circular = view.findViewById(R.id.progress_circular);

        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                notification.setVisibility(View.GONE);
                if (!validate())
                    return;
                view.setVisibility(View.GONE);
                progress_circular.start();

                OkHttpClient client = new OkHttpClient();

                JSONObject postdata = new JSONObject();
                try {
                    postdata.put("client_id", "chinesepodrecap");
                    postdata.put("username", email.getText());
                    postdata.put("signature", sha256(client_secret + password.getText()));
                } catch(JSONException e){
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

                MediaType MEDIA_TYPE = MediaType.parse("application/x-www-form-urlencoded");
                RequestBody body = RequestBody.create(MEDIA_TYPE, postdata.toString());

                Request request = new Request.Builder()
                        .url(Config.LOGIN)
                        .post(body)
                        .header("Content-Type", "application/x-www-form-urlencoded")
                        .build();

                client.newCall(request).enqueue(new Callback() {
                    @Override
                    public void onFailure(Call call, IOException e) {
                        String mMessage = e.getMessage().toString();
                        Log.w("failure Response", mMessage);
                        //call.cancel();
//                        mHandler.sendEmptyMessageDelayed(0, 750);
                        showLoginHandler.sendEmptyMessage(0);
                    }

                    @Override
                    public void onResponse(Call call, Response response) throws IOException {

                        String jsonData = response.body().string();
                        try {
                            JSONObject obj = new JSONObject(jsonData);
                            int success = obj.getInt("success");
                            if (success == 1){
                                String session_id = obj.getJSONObject("data").getString("sessionid");

                                checkSessionId(session_id);
                            } else {
                               notifyHandler.sendEmptyMessage(0);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            notifyHandler.sendEmptyMessage(0);
                        }
                        response.close();
                    }
                });
            }
        });

        return view;
    }

    private Boolean validate() {
        email_layout.setError(null);
        password_layout.setError(null);
        int errorCnt = 0;
        if (email.getText().toString().isEmpty()){
            email_layout.setError("Email is required");
            errorCnt++;
        }
        if (password.getText().toString().isEmpty()){
            password_layout.setError("Password is required");
            errorCnt++;
        }
        if (errorCnt > 0){
            return false;
        }
        return true;
    }

    public static String sha256(String base) {
        try{
            MessageDigest digest = MessageDigest.getInstance("SHA-256");
            byte[] hash = digest.digest(base.getBytes("UTF-8"));
            StringBuffer hexString = new StringBuffer();

            for (int i = 0; i < hash.length; i++) {
                String hex = Integer.toHexString(0xff & hash[i]);
                if(hex.length() == 1) hexString.append('0');
                hexString.append(hex);
            }

            return hexString.toString();
        } catch(Exception ex){
            throw new RuntimeException(ex);
        }
    }

    private void checkSessionId(String sessionId){
        HttpUrl.Builder urlBuilder
                = HttpUrl.parse(Config.CHECK_SESSION_ID).newBuilder();
        urlBuilder.addQueryParameter("sessionid", sessionId);

        String url = urlBuilder.build().toString();

        Request request = new Request.Builder()
                .url(url)
                .build();

        OkHttpClient client = new OkHttpClient();
        Call call = client.newCall(request);
        client.newCall(request).enqueue(new okhttp3.Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                String mMessage = e.getMessage().toString();
                Log.w("failure Response", mMessage);
                //call.cancel();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {

                String jsonData = response.body().string();
                try {
                    JSONObject obj = new JSONObject(jsonData);
                    int success = obj.getInt("success");
                    if (success == 1){
                        SharedPreferences sharedPreferences = getActivity().getSharedPreferences("user_info", MODE_PRIVATE);
                        SharedPreferences.Editor editor = sharedPreferences.edit();
                        editor.putString("sessionid", sessionId);
                        editor.commit();

                        startActivity(new Intent(getActivity(), MainActivity.class));
                        getActivity().finish();
                    }else {
                        notifyHandler.sendEmptyMessage(0);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                response.close();
            }
        });
    }

    private void vibrate() {
        Vibrator vibrator = (Vibrator) getActivity().getSystemService(Context.VIBRATOR_SERVICE);
        // Vibrate for 500 milliseconds
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            vibrator.vibrate(VibrationEffect.createOneShot(500, VibrationEffect.DEFAULT_AMPLITUDE));
        } else {
            //deprecated in API 26
            vibrator.vibrate(500);
        }
    }

    Handler notifyHandler = new Handler(){
        @Override
        public void handleMessage(@NonNull Message msg) {
            notification.setVisibility(View.VISIBLE);

            Animation shake = AnimationUtils.loadAnimation(getActivity(), R.anim.shake);
            shake.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {

                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    showLoginHandler.sendEmptyMessage(0);
                }

                @Override
                public void onAnimationRepeat(Animation animation) {

                }
            });
            notification.startAnimation(shake);
            vibrate();
        }
    };
     Handler showLoginHandler = new Handler(){
         @Override
         public void handleMessage(@NonNull Message msg) {
             login.setVisibility(View.VISIBLE);
             progress_circular.stop();
         }
     };
}
